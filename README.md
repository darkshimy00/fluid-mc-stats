# Fluid MC Stats

A web interface for the [Lolmewn Stats plugin on Bukkit Dev](http://accpro.ws/ILBg2).

Coded by [DevelopGravity](http://developgravity.com) and [Lolmewn](http://accpro.ws/R2CMf).

- [Official Website](http://developgravity.com/projects/fluid-mc-stats/)
- [Official Forums](http://developgravity.com/forums/board/project-forums/fluid-mc-stats/)

# Requirements

- LAMP Server (Linux, Apache, MySQL/MariaDB, PHP) Create one [here](http://accpro.ws/-CP5d) for $5/month.
- RewriteRules enabled (Coming Soon!).
- A Minecraft server running the [Stats latest plugin](http://accpro.ws/ILBg2).

# Notices

- Before making an issue, please read the `CONTRIBUTING.md` file.
- If you are using Nginx, we will not answer support unless the problem you have is reproducible in Apache.
- Please do not download the `master` branch, we will not support issues for these. Always download via a tagged version.
- IF YOUR PLAYER IMAGES ARE NOT LOADING, PLEASE UPDATE TO THE LATEST AND DO NOT MAKE A ISSUE ABOUT IT. WE HAVE [MENTIONED](http://developgravity.com/2014/03/notice-to-our-minecraft-avatar-service-users/) THAT WE ARE DISCONTINUING OUR OLD IMAGE SERVICE URL.
- We are now working on the new interface. This will contain advanced stats and a way to change the looks to it (Via the Bootstrap CSS, 10+ themes included). 
- Support for the `v0.1.X` series will now be discontinued as we are now working on v0.2.0 that will contain advanced statistics, charts, theming via Bootstrap themes, and much more. 

# News

- 14-04-22 [Fluid MC Stats Beta v0.1.3 Update Notes](http://developgravity.com/2014/04/fluid-mc-stats-beta-v0-1-3-update-notes/)
- 14-04-19 [Fluid MC Stats Beta v0.1.2 Update Notes](http://developgravity.com/2014/04/fluid-mc-stats-beta-v0-1-2-update-notes/)
- 14-03-12 [Fluid MC Stats Beta v0.1.1 Update Notes](http://developgravity.com/2014/03/fluid-mc-stats-beta-v0-1-1-update-notes/)
- 14-03-09 [Notice to Our Minecraft Avatar Service Users](http://developgravity.com/2014/03/notice-to-our-minecraft-avatar-service-users/)
- 14-03-01 [Fluid MC Stats Beta v0.1.0 Is Now Out](http://developgravity.com/2014/03/fluid-mc-stats-beta-v0-1-0-is-now-out/)

*Want to get email updates about changes? Go to [developgravity.com](http://developgravity.com) and add your email to the list via the sidebar!*

# Feedback

Feedback is greatly appreciated and can help us come up with ideas for feature releases. If you would like to provide some feedback, please fill out the [survey](http://accpro.ws/4tbNe).

# License

All rights reserved. You may not remove branding (AccountProductions, DevelopGravity, Lolmewn, and their respected links) or modify it other than editing the configuration. See `LICENSE.md` for more information.


Copyright (c) AccountProductions and Lolmewn 2014. All Rights Reserved.
